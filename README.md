# Equalizing Panels
Cross Functional Workshop to provide a solution for equializing panels height.

## Getting Started
You just have to clone a copy into your local computer and NPM install dependencies.
Project will preload a set of equalized panels using a mock JSON file.

### Prerequisites
NODE JS, RUBY, SASS, GRUNT.

### Installing
1. Clone into your local environment.
2. Run NPM Install to setup dependencies.
3. Use a command window and trigger Grunt command to open a local server.

### How to Equalize panels
This is up to you my friend.

## Authors
Gibran Villarreal Diaz

## License
Open Source

